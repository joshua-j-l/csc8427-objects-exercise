public class Pet {
    private String name;
    private String species;

    //constructor
    public Pet(String name, String species){
        this.name = name;
        this.species = species;
    }

    //Method
    public String getName(){
        return name;
    }

    public  String getSpecies(){
        return species;
    }

}
