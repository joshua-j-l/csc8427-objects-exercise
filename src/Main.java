public class Main {

    public static void main(String[] args) {

        //creating and pet and person object here, from a class!
        //see class as a template --> class template
        //when we have a variable that is an object, we need to be initialised before they can be used.
        Pet marvin = new Pet("Marvin", "Dog");
        Pet axolotl = new Pet("Axolotl", "Salamander");
        // need to initialise pet objects before using them with people objects

//        LanParty party = new LanParty();
//        party[0] = 1;
//        party[1] = 2;
//        party[2] = 3;

        Person bob = new Person("Bob", "Smith", null, 4);
        Person sarah = new Person("Sarah", "Smith", null, 48);
        Person jeremiah = new Person("Jeremiah", "Obadiah", null, 4827254);

        sarah.setMyPet(marvin);

        Person[] personListEmpty = new Person[3];

        personListEmpty[0] = bob;

        // adding person to empty space in list
        for (int i = 0; i < 3; i++) {
            if (personListEmpty[i] == null) {
                personListEmpty[i] = sarah;
                break;
            } else {
                System.out.println("space occupied by " + personListEmpty[i].getFirstName());
            }
        }

        System.out.println("Print names in list: ");
        for (int i = 0; i < 3; i++) {
            if (personListEmpty[i] == null) {
                System.out.println("empty space in list");
            } else {
                System.out.println(personListEmpty[i].getFirstName());
            }
        }

        //get list filled with nulls
        //add people individually - loop through list to take the first null value

        System.out.println();
        System.out.println(bob.getFullName());
        System.out.println(sarah.getFullName());

        //writing sarah gives us access to that variable, which is an object
        //can now access methods available to that object
        //below changes an attribute of that object
        sarah.setSurname("Jones");
        System.out.println(sarah.getFullName());

        //creating another variable of class Pet called theirPet
        // looking in the sarah object, finding the pet attribute and returning it
        //below is a safety if statement, in case pet object hasnt been initialised and remains null
        Pet theirPet = sarah.getMyPet();
        if (theirPet == null) {
            System.out.println(sarah.getFirstName() + " has no pet");
        } else {
            System.out.println(sarah.getFirstName() + " has a pet called " + theirPet.getName());
        }
    }
}
