// create a class called person
//name has to be the same as the .java class
//convention is that we add elements to the class  in the following sequence -> attributes, constructor, methods
public class Person {

    // attributes
    //encapsulating attributes within our class so we are only allowing people to access them safely
    // so, declaring attributes as private
    // also, using a custom class as an attribute here
    private String firstName;
    private String surname;
    private Pet myPet;
    private Integer yuGiOhCards;

    //constructor
    // contains access modifier, name of class, parameters
    // use different names for the parameters relative to attributes
    // setting parameters to equal attributes
    //when we create a person, we do not specify what their pet is. If we dont tell the code what the pet is, like
    // with sarah.setMyPet(marvin), we will get error.
    //myPet hasnt been initialised, so will be null value, so will get a NullPointerException
    public Person(String fName, String sName, Pet mPet, Integer ygoCards) {
        this.firstName = fName;
        this.surname = sName;
        this.myPet = mPet;
        this.yuGiOhCards = ygoCards;
    }


    //methods
    //access methods allow people to access the attributes
    // .this makes it clear that we are accessing attributes of THIS object
    // getters
    public String getFirstName() {
        return this.firstName;
    }

    public String getSurname() {
        return surname;
    }


    //when we have a variable that is an object, we need to be initialised before they can be used.
    public Pet getMyPet(){
        return myPet;
    }

    public String getFullName() {
        return firstName + " " + surname;
    }


    //Setters

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setMyPet(Pet myPet) {
        this.myPet = myPet;
    }
}
