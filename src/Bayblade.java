public class Bayblade {

    // attributes
    private String buildType;
    private Boolean extraSpeed;

    //constructor
    public Bayblade(String type, Boolean xSpeed) {
        this.buildType = type;
        this.extraSpeed = xSpeed;
    }

    //Method
    public String getType() {
        return buildType;
    }

    public Boolean getSpeed() {
        return extraSpeed;
    }

}
